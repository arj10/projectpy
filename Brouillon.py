import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.graphics.mosaicplot import mosaic
import datetime
import Costumers as cost
"""
data = pd.read_table('Coffeebar_2013-2017.csv', sep=';')
data['FOOD'] = data['FOOD'].replace(np.nan, '.')

# Drinks served
# print(pd.unique(data['DRINKS']))

# Food served
data_no_nan = data[data["FOOD"] != '.']
# print(pd.unique(data_no_nan['FOOD']))


# Plots
plt.figure(figsize=(20, 10))
plt.subplot(1, 2, 1)
plt.title("Drinks")
data["DRINKS"].value_counts().plot(kind='bar')

plt.subplot(1, 2, 2)
plt.title("Foods")
data_no_nan["FOOD"].value_counts().plot(kind='bar')


plt.show()

# Mosaic plot
mosaic(data, ["DRINKS", "FOOD"])
plt.show()

SubData = np.where(data["TIME"][-8:] == '08:00:00')
print(SubData)
print(data["TIME"][-8:])


MyList = (data['TIME'])

i = " "
MyList2 = []

for i in MyList:
    hour = i[-8:-6]
    minute = i[-5:-3]
    now = datetime.time(hour=int(hour), minute=int(minute))
    MyList2.append(now)

data['HOURS'] = MyList2
print(MyList2[1].type)


MatrixDrinks = pd.crosstab(data['HOURS'], data['DRINKS'], normalize='index')
print(MatrixDrinks)
MatrixFoods = pd.crosstab(data['HOURS'], data['FOOD'], normalize='index')
print(MatrixFoods)
"""

list = []
for i in range(1, 5):
    new_costumer = cost.Customer('Hipster')
    list.append(new_costumer)
    print(new_costumer.ID)
for i in range(5, 10):
    new_costumer = cost.Customer('Returning')
    list.append(new_costumer)
    print(new_costumer.ID)

list = [x for x in list if x.ID != 5]

print("")
print(list[0].ID)
print("")
for i in range(1, 8):
    print(list[i].ID)
