import pandas as pd
import Costumers as cost
import random
import datetime
import matplotlib.pyplot as plt


# given the previous time & date as parameters, this function will generate the next time slot
def get_time(previous_time):

    if previous_time.hour == 17 and previous_time.minute == 56:  # end of the day then generate the next day
        ahead_time = datetime.datetime(year=previous_time.year, month=previous_time.month, day=previous_time.day,
                                       hour=8, minute=0) + datetime.timedelta(days=float(1))
    else:
        now = datetime.datetime(year=previous_time.year, month=previous_time.month, day=previous_time.day,
                                hour=previous_time.hour, minute=previous_time.minute)
        if previous_time.hour <= 10:  # between 8 am and 10.59 am add each time + 5 minutes
            delta_minute = 5
        elif previous_time.hour <= 12:  # between 11 and 12.59 pm add each time + 2 minutes
            delta_minute = 2
        else:
            delta_minute = 4  # between 13 pm and 18 pm add each time + 4 minutes
        ahead_time = now + datetime.timedelta(minutes=delta_minute)

    return ahead_time


# here is a list of returning costumers. 1/3 hipsters
returning_costumers = []
for i in range(1, 334):
    returning_costumers.append(cost.Customer('Hipster'))

for i in range(334, 1001):
    returning_costumers.append(cost.Customer('Returning'))


# this function doesn't take any parameters and selects randomly a costumer (either in the returning costumer list)
def generate_costumer():

    pr = random.uniform(0, 1)
    if pr <= 0.2:  # 20% chance to get a random returning customer
        pr_returning = int(random.uniform(1, len(returning_costumers)))  # select randomly a returning costumer
        return returning_costumers[pr_returning]  # return a random returning costumer
    else:
        pr_one_time = random.uniform(0, 1)
        if pr_one_time <= 0.1:  # within the one time customer, 10% are TripAdvisor
            new_costumer = cost.Customer('TripAdvisor')
        else:
            new_costumer = cost.Customer('OneTime')

        return new_costumer  # return a new costumer


first_time_slot = datetime.datetime(year=2018, month=1, day=1, hour=8, minute=0) # simulation start date
last_time_slot = datetime.datetime(year=2022, month=12, day=31, hour=17, minute=56) # simulation end date
simulation_list = []

print('This process could take some time to run (approx 11 min on my computer)')


while first_time_slot != last_time_slot:
    time_only = datetime.time(hour=first_time_slot.hour, minute=first_time_slot.minute) # get only the hour and minutes
    costumer = generate_costumer()

    # line will have all the data regarding one sell (customer)
    line = [first_time_slot, costumer.ID, costumer.type, cost.Customer.buy_drink(costumer, str(time_only)),
            cost.Customer.buy_food(costumer, str(time_only)), costumer.tip, costumer.budget]

    # we'll add into 'simulation_list' one by one each 'line'
    simulation_list.append(line)

    # let's look whether the costumer still has some budget left
    # if not we'll erase him from the list of returning costumers
    if costumer.budget < 5:  # 5 being the most expensive item of the coffee shop
        returning_costumers = [client for client in returning_costumers if client.ID != costumer.ID]

    next_time_slot = get_time(first_time_slot)
    first_time_slot = next_time_slot  # generate the next time with the previous until start date = end date

Simulation_df = pd.DataFrame(columns=['Time', 'CostumerID', 'CostumerType', 'Drink', 'Food', 'Tips',
                                      'Remaining_Budget'], data=simulation_list)

# Simulation data is stored directly into a csv file
Simulation_df.to_csv('Simulation_result.csv')

# Bar plot of the total amount of sold drinks
plt.figure(figsize=(30, 30))
plt.subplot(1, 2, 1)
plt.title("Total amount of sold drinks")
Simulation_df['Drink'].value_counts().plot(kind='bar')

# Bar plot of the total amount of sold foods
plt.subplot(1, 2, 2)
plt.title("Total amount of sold foods")
Simulation_df['Food'].value_counts().plot(kind='bar')
plt.show()