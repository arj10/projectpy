import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.graphics.mosaicplot import mosaic
import datetime

data = pd.read_table('Coffeebar_2013-2017.csv', sep=';')
data.head()
print(data)

# Type of Food

data["FOOD"] = data["FOOD"].replace(np.nan, ".")
index1 = data.columns.get_loc("FOOD")
data_no_nan = data[data["FOOD"] != '.']
data_food = data_no_nan.iloc[:, index1]
print("The food served in the bar is :")
print(pd.unique(data_food))

# Type of Drinks

index2 = data.columns.get_loc("DRINKS")
data_drinks = data.iloc[:, index2]
print("The drinks served are :")
print(pd.unique(data_drinks))

# Number of unique customer

index3 = data.columns.get_loc("CUSTOMER")
data_customer = data.iloc[:, index3]
data_customer_unique = pd.unique(data_customer)
print("The number of unique customer is :")
print(data_customer_unique.size)

# Bar plot of the total amount of sold drinks

plt.figure(figsize=(30, 30))
plt.subplot(1, 2, 1)
plt.title("Total amount of sold drinks")
data["DRINKS"].value_counts().plot(kind='bar')


# Bar plot of the total amount of sold foods

plt.subplot(1, 2, 2)
plt.title("Total amount of sold foods")
data_food.value_counts().plot(kind='bar')
plt.show()

# Mosaic of the Repartition of the consumption of drinks and foods

mosaic(data, ["DRINKS", "FOOD"])
plt.title("Repartition of the consumption of drinks and foods")
plt.show()

# Probabilities

i = " "
temporary_list = []
time_list = (data['TIME'])

# extract time without dates to compute probabilities
for i in time_list:
    hour = i[-8:-6]
    minute = i[-5:-3]
    now = datetime.time(hour=int(hour), minute=int(minute))
    temporary_list.append(now)

data['HOURS'] = temporary_list


# Create matrices

Matrix_proba_Drinks = pd.crosstab(data["HOURS"], data["DRINKS"], normalize='index')
print(Matrix_proba_Drinks)

Matrix_proba_Food = pd.crosstab(data["HOURS"], data["FOOD"], normalize='index')
print(Matrix_proba_Food)

# In order to use the probability matrices afterwards
# Export matrices

Matrix_proba_Drinks.to_csv('Matrix_Drinks.csv')
Matrix_proba_Food.to_csv('Matrix_Food.csv')
