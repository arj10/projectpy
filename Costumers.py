import pandas as pd
import random

# Import probability matrices generated in the Explanatory part

DrinkData = pd.read_table('Matrix_Drinks.csv', sep=',', names=
                          ["Hours", "Coffee", "Frappucino", "Milkshake", "Soda", "Tea", "Water"], header=0)
FoodData = pd.read_table('Matrix_Food.csv', sep=',', names=
                         ["Hours", "Nothing", "Cookie", "Muffin", "Pie", "Sandwich"], header=0)

# Creation of the Customers with their different characteristics (budget , tip)


class Customer(object):
    NbOfCustomer = 0

    def __init__(self, c_type):
        Customer.NbOfCustomer += 1  # to get unique Customer ID
        self.ID = Customer.NbOfCustomer
        if c_type == 'OneTime':
            self.budget = 100
            self.tip = 0
            self.type = c_type
        elif c_type == 'TripAdvisor':
            self.budget = 100
            self.tip = random.randint(1, 10)
            self.type = c_type
        elif c_type == 'Returning':
            self.budget = 250
            self.tip = 0
            self.type = c_type
        elif c_type == 'Hipster':
            self.budget = 500
            self.tip = 0
            self.type = c_type

    def buy_drink(self, time):
        temp_line1 = DrinkData.loc[DrinkData['Hours'] == time] # Take the time in the DrinkData matrix
        pr = random.uniform(0, 1) # Generate a random probability pr between 0 and 1

# Assign the consumption probability from the drinks matrix corresponding to the time of arrival for each drink
        pr_coffee = float(temp_line1['Coffee'])
        pr_frap = float(temp_line1['Frappucino'])
        pr_milk = float(temp_line1['Milkshake'])
        pr_soda = float(temp_line1['Soda'])
        pr_tea = float(temp_line1['Tea'])

# Comparison of the random probability pr to the cumulative probability of the drinks pr_XXX in order to return the
        # appropriate output (drink)
# Each time we add another drink it increases the cumulative probability and allows to see in which cumulative
        # probability interval is located pr

        if pr <= pr_coffee:
            if self.budget >= 3: # Check if the customer has enough budget
                self.budget = self.budget - 3  # Substract the corresponding purchase price to the budget
                return 'Coffee'
            else:
                return 'Nothing'
        elif pr <= (pr_coffee + pr_frap):
            if self.budget >= 4:
                self.budget = self.budget - 4
                return 'Frappucino'
            else:
                return 'Nothing'
        elif pr <= pr_coffee + pr_frap + pr_milk:
            if self.budget >= 5:
                self.budget = self.budget - 5
                return 'Milkshake'
            else:
                return 'Nothing'
        elif pr <= pr_coffee + pr_frap + pr_milk + pr_soda:
            if self.budget >= 3:
                self.budget = self.budget - 3
                return 'Soda'
            else:
                return 'Nothing'
        elif pr <= pr_coffee + pr_frap + pr_milk + pr_soda + pr_tea:
            if self.budget >= 3:
                self.budget = self.budget - 3
                return 'Tea'
            else:
                return 'Nothing'
        else:
            if self.budget >= 2:
                self.budget = self.budget - 2
                return 'Water'
            else:
                return 'Nothing'

    def buy_food(self, time):
        temp_line2 = FoodData.loc[FoodData['Hours'] == time]  # Take the time in the FoodData matrix
        pr = random.uniform(0, 1)  # Generate a random probability pr between 0 and 1

# Assign the consumption probability from the food matrix corresponding to the time of arrival for each food

        pr_nothing = float(temp_line2['Nothing'])
        pr_cookie = float(temp_line2['Cookie'])
        pr_muffin = float(temp_line2['Muffin'])
        pr_pie = float(temp_line2['Pie'])

# Comparison of the random probability pr to the cumulative probability of the foods pr_XXX in order to return the
        #  appropriate output (food)
# Each time we add another food it increases the cumulative probability and allows to see in which cumulative
        # probability interval is located pr

        if pr <= pr_nothing:
            return 'Nothing'
        elif pr <= pr_nothing + pr_cookie:
            if self.budget >= 2:
                self.budget = self.budget - 2
                return 'Cookie'
            else:
                return 'Nothing'
        elif pr <= pr_nothing + pr_cookie + pr_muffin:
            if self.budget >= 3:
                self.budget = self.budget - 3
                return 'Muffin'
            else:
                return 'Nothing'
        elif pr <= pr_nothing + pr_cookie + pr_muffin + pr_pie:
            if self.budget >= 3:
                self.budget = self.budget - 3
                return 'Pie'
            else:
                return 'Nothing'
        else:
            if self.budget >= 5:
                self.budget = self.budget - 5
                return 'Sandwich'
            else:
                return 'Nothing'




